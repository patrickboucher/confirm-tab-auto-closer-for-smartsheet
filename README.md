# Confirm tab auto closer for Smartsheet

Extension for the Smartsheet application (see smartsheet.com) deployed for
Unigrav Inc. This code closes a Smartsheet form tab when the confirmation page
is displayed. When the URL matches a confirmation address, the tab closes
after a delay of 5 seconds.

### Compatibility

It was made to run on Google Chrome and Brave browsers.

### Installation

1. Download and extract this project.
2. Activate developer mode on your browser's extension module.
3. Click on "Load unpacked" and select this project's folder.

Voila!
