/*
Extension for the Smartsheet application (see smartsheet.com) deployed for
Unigrav Inc. This code closes a Smartsheet form tab when the confirmation page
is displayed. When the URL matches a confirmation address, the tab closes
after a delay of 5 seconds.

Author: Patrick Boucher
Date: june 6, 2020
Version: 1.0
Licence: MIT
*/

var formPartialUrl = "https://app.smartsheet.com/b/form/"
var confirmPrefix = "?confirm=true";

/**
 * Event that occurs each time a tab is updated. If the page is loaded and the
 * URL corresponds to a confirmation page, it closes the tab after 5 seconds.
 */
chrome.tabs.onUpdated.addListener(
    function(tabId, changedInfo, tab) {
        if (changedInfo.status === "complete") {
            let url = tab.url;
            if (isSmartSheetConfirmPage(url)) {
                setTimeout(closeTab, 5000, tab);
            }
        }
    }
);

/**
 * Verify that the url corresponds to a Smartsheet form confirmation page.
 * @param {string} url The tab's url.
 * @returns {boolean} True if it is a Smartsheet confirmation page.
 */
function isSmartSheetConfirmPage(url) {
    return url.includes(formPartialUrl) && url.includes(confirmPrefix);
}

/**
 * Closes a tab.
 * @param {Tab} tab 
 */
function closeTab(tab) {
    chrome.tabs.remove(tab.id);
}